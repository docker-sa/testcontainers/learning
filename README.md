# learning

## Start the WebApp
```bash
cd webapp
docker compose up
```

- Open the browser and go to `http://localhost:6065`
- Call the "counter API" with `curl http://localhost:6065/counter`

## Run test container

```bash
cd webapp
go test
````
